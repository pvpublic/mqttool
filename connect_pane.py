from custom_widgets import MySwitch, PortEntry, WidgetBoxWithLabel
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class HostPortBox(Gtk.Grid):
    def __init__(self):
        Gtk.Grid.__init__(self)
        
        self.hostField = Gtk.Entry()
        self.hostField.set_placeholder_text("Host")
        self.hostField.set_size_request(200, -1)
        self.attach(self.hostField, 1, 1, 1, 1)

        self.portField = PortEntry()
        self.attach_next_to(self.portField, self.hostField, Gtk.PositionType.RIGHT, 1, 1)

class TlsParamsBox(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, expand=False, spacing=10)
        self.styleContext = Gtk.Box.get_style_context(self)
        self.styleContext.add_class("connectPaneBox")

        self.caChooseBtn = Gtk.FileChooserButton(title="Choose CA Certificate file", action=Gtk.FileChooserAction.OPEN, expand=False)
        self.caChooseBox = WidgetBoxWithLabel("CA Certificate", self.caChooseBtn, expandable=False)
        self.pack_start(self.caChooseBox, False, False, 0)

        self.keyChooseBtn = Gtk.FileChooserButton(title="Choose PEM Private Key file", action=Gtk.FileChooserAction.OPEN, expand=False)
        self.keyChooseBox = WidgetBoxWithLabel("Private Key", self.keyChooseBtn, expandable=False)
        self.pack_start(self.keyChooseBox, False, False, 0)

        self.clChooseBtn = Gtk.FileChooserButton(title="Choose PEM Client Certificate file", action=Gtk.FileChooserAction.OPEN, expand=False)
        self.clChooseBox = WidgetBoxWithLabel("Client Certificate", self.clChooseBtn, expandable=False)
        self.pack_start(self.clChooseBox, False, False, 0)

    def caCertPath(self):
        return self.caChooseBtn.get_filename()
    def keyCertPath(self):
        return self.keyChooseBtn.get_filename()
    def clientCertPath(self):
        return self.clChooseBtn.get_filename()

class UsernamePasswdBox(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, expand=False, spacing=10)
        self.styleContext = Gtk.Box.get_style_context(self)
        self.styleContext.add_class("connectPaneBox")

        self.usernameEntry = Gtk.Entry()
        self.usernameEntry.set_placeholder_text("Username")
        self.pack_start(self.usernameEntry, False, False, 0)

        self.pswdEntry = Gtk.Entry()
        self.pswdEntry.set_placeholder_text("Password")
        self.pswdEntry.set_visibility(False)
        self.pack_start(self.pswdEntry, False, False, 0)
    
    def username(self):
        return self.usernameEntry.get_text()
    def password(self):
        return self.pswdEntry.get_text()

class ConnectPane(Gtk.Grid):
    def __init__(self):
        Gtk.Grid.__init__(self, expand=True)
        self.topRow = Gtk.Box(spacing=3)

        self.connectBtn = Gtk.Button(expand=False)
        self.connectBtn.set_label("Connect")
        self.connectBtn.set_size_request(-1, -1)
        
        self.connectionParamsBox = HostPortBox()
        self.topRow.pack_start(self.connectionParamsBox, True, True, 0)
        self.topRow.pack_end(self.connectBtn, False, False, 3)
        self.topRow.set_homogeneous(False)
        self.attach(self.topRow, 1, 1, 1, 1)

        self.tlsSwitch = MySwitch()
        self.tlsSwitch.connect("notify::active", self.onTlsSwitchToggled)
        self.tlsSwitchBox = WidgetBoxWithLabel("Use SSL/TLS", self.tlsSwitch)
        self.attach_next_to(self.tlsSwitchBox, self.topRow, Gtk.PositionType.BOTTOM, 1, 1)

        self.tlsParams = TlsParamsBox()
        self.tlsParams.set_sensitive(False)
        self.attach_next_to(self.tlsParams, self.tlsSwitchBox, Gtk.PositionType.BOTTOM, 1, 1)

        self.pswdSwitch = MySwitch()
        self.pswdSwitch.connect("notify::active", self.onPswdSwitchToggled)
        self.pswdSwitchBox = WidgetBoxWithLabel("Use Username and Password", self.pswdSwitch)
        self.attach_next_to(self.pswdSwitchBox, self.tlsParams, Gtk.PositionType.BOTTOM, 1, 1)

        self.pswdParams = UsernamePasswdBox()
        self.pswdParams.set_sensitive(False)
        self.attach_next_to(self.pswdParams, self.pswdSwitchBox, Gtk.PositionType.BOTTOM, 1, 1)

    def getHost(self):
        return self.connectionParamsBox.hostField.get_text()
        
    def getPort(self):
        return self.connectionParamsBox.portField.get_value()

    def setSensitivityOnConnState(self, connected):
        widgetsToSet = [self.connectionParamsBox, self.tlsParams, self.tlsSwitch, self.pswdParams, self.pswdSwitch]
        for w in widgetsToSet:
            w.set_sensitive(not connected)
    
    def onTlsSwitchToggled(self, switch, _):
        self.tlsParams.set_sensitive(switch.get_state())
    def onPswdSwitchToggled(self, switch, _):
        self.pswdParams.set_sensitive(switch.get_state())

    def tlsEnabled(self):
        return self.tlsSwitch.get_state()
    def caCertPath(self):
        return self.tlsParams.caCertPath()
    def keyCertPath(self):
        return self.tlsParams.keyCertPath()
    def clientCertPath(self):
        return self.tlsParams.clientCertPath()
    
    def pswdEnabled(self):
        return self.pswdSwitch.get_state()
    def username(self):
        return self.pswdParams.username()
    def password(self):
        return self.pswdParams.password()
    
