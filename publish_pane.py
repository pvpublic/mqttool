import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from custom_widgets import TopicEntry, QosSelector, WidgetBoxWithLabel

class PublishPane(Gtk.Grid):
    def __init__(self):
        Gtk.Grid.__init__(self)

        self.topicField = TopicEntry(expand=False)
        self.attach(self.topicField, 1, 1, 3, 1)

        self.payloadField = Gtk.Entry(expand=True)
        self.payloadField.set_placeholder_text("Payload")
        self.attach_next_to(self.payloadField, self.topicField, Gtk.PositionType.BOTTOM, 3, 1)

        self.retainCheckbox = Gtk.CheckButton()
        self.retainBox = WidgetBoxWithLabel("Retain", self.retainCheckbox)
        self.attach_next_to(self.retainBox, self.payloadField, Gtk.PositionType.BOTTOM, 1, 1)

        self.qosSelector = QosSelector()
        self.attach_next_to(self.qosSelector, self.retainBox, Gtk.PositionType.RIGHT, 1, 1)

        self.publishBtn = Gtk.Button(expand=False)
        self.publishBtn.set_label("Publish")
        self.publishBtn.set_border_width(5)
        self.attach_next_to(self.publishBtn, self.qosSelector, Gtk.PositionType.RIGHT, 1, 1)
