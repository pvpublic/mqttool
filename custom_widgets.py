import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class TopicEntry(Gtk.Entry):
    def __init__(self, **kwargs):
        Gtk.Entry.__init__(self, **kwargs)
        self.set_placeholder_text("Topic")
    
    def valid(self):
        if self.get_text() == "":
            return False
        return True

class PortEntry(Gtk.Entry):
    def __init__(self, **kwargs):
        Gtk.Entry.__init__(self, **kwargs)
        self.set_text("1883")
        self.connect("focus-out-event", self.makeValid)
        self.set_placeholder_text("Port")
        self.set_size_request(70, -1)
        self.set_max_length(5)
    
    def get_value(self):
        return int(Gtk.Entry.get_text(self))
    
    def set_value(self, val):
        Gtk.Entry.set_text(self, str(val))
    
    def makeValid(self, event, _):
        oldtext = self.get_text()
        newtext = ""
        for char in oldtext:
            if char.isnumeric():
                newtext += char
        if newtext != "":
            if int(newtext) > 65535:
                newtext = "1883"
        self.set_value(newtext)

class QosSelector(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, spacing=3)
        self.qosLabel = Gtk.Label(label="QoS")
        self.qosLabel.set_justify(Gtk.Justification.RIGHT)
        self.pack_start(self.qosLabel, False, False, 0)
        self.qosSelector = Gtk.ComboBoxText()
        self.qosSelector.set_entry_text_column(0)
        for i in range(0, 3):
            self.qosSelector.append_text(str(i))
        self.qosSelector.set_active(0)
        self.pack_start(self.qosSelector, False, False, 0)
    
    def value(self):
        return int(self.qosSelector.get_model()[self.qosSelector.get_active_iter()][0])

class WidgetBoxWithLabel(Gtk.Box):
    def __init__(self, labelText, widget, expandable=False):
        Gtk.Box.__init__(self, expand=expandable, spacing=5)
        self.pack_start(Gtk.Label(label=labelText), False, False, 0)
        self.pack_start(widget, False, False, 0)

class MySwitch(Gtk.Switch):
    def __init__(self):
        Gtk.Switch.__init__(self, expand=False)
        self.set_size_request(-1, -1)
        self.set_active(False)