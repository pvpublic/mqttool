import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, Gio, GLib


class BottomBar(Gtk.ActionBar):
    def __init__(self):
        Gtk.ActionBar.__init__(self)

        self.messageLabel = Gtk.Label()
        self.messageLabel.set_text("Welcome to MQTTool!")
        self.spinner = Gtk.Spinner()
        self.pack_start(self.spinner)
        self.set_center_widget(self.messageLabel)
        self.set_border_width(10)
    
    def start(self, text):
        self.messageLabel.set_text(text)
        self.spinner.start()
    
    def stop(self, text):
        self.messageLabel.set_text(text)
        self.spinner.stop()
