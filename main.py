#!/usr/bin/python3

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, Gio, GLib

import paho.mqtt.client as mqtt
from time import sleep
from threading import Thread
from publish_pane import PublishPane
from subscribe_pane import SubscribePane
from connect_pane import ConnectPane
from bottom_bar import BottomBar

class MainWindow(Gtk.Window):
    def __init__(self):
        self.client = mqtt.Client(client_id="randomid")
        self.connected = False
        self.running = True

        Gtk.Window.__init__(self, title="MQTTool")
        GLib.set_application_name("MQTTool")
        self.set_size_request(800, 600)
        self.connect("destroy", Gtk.main_quit)
        self.connect("destroy", self.exitCleanly)
        self.mainGrid = Gtk.Grid()
        self.mainGrid.set_name("mainGrid")
        self.add(self.mainGrid)
        self.loadStylesheet("mqttool.css")

        self.header = Gtk.HeaderBar()
        self.header.set_show_close_button(True)
        self.set_titlebar(self.header)

        self.stack = Gtk.Stack()
        self.connectPane = ConnectPane()
        self.stack.add_titled(self.connectPane, "ConnectPane", "Connect")
        self.publishPane = PublishPane()
        self.stack.add_titled(self.publishPane, "PublishPane", "Publish")
        self.subscribePane = SubscribePane()
        self.stack.add_titled(self.subscribePane, "SubscribePane", "Subscribe")
        self.subscribePane.set_sensitive(False)
        self.stack.set_transition_duration(200)
        self.stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        self.stack.set_interpolate_size(True)
        self.mainGrid.attach(self.stack, 1, 1, 1, 1)

        self.stackSwitcher = Gtk.StackSwitcher()
        self.stackSwitcher.set_stack(self.stack)
        self.header.set_custom_title(self.stackSwitcher)

        self.bottomSeparator = Gtk.Separator.new(Gtk.Orientation.HORIZONTAL)
        self.mainGrid.attach_next_to(self.bottomSeparator, self.stack, Gtk.PositionType.BOTTOM, 1, 1)

        self.connectPane.connectBtn.connect("pressed", self.toggleConnection)
        self.publishPane.publishBtn.connect("pressed", self.publish)
        self.subscribePane.subscribeBtn.connect("pressed", self.subscribe)
        self.subscribePane.unsubscribeBtn.connect("pressed", self.unsubscribe)

        self.bottomBar = BottomBar()
        self.mainGrid.attach_next_to(self.bottomBar, self.bottomSeparator, Gtk.PositionType.BOTTOM, 1, 1)
        
        self.setWidgetSensitivities()

        self.mqttLoopThread = Thread(target=self.mqttLoop)
        self.mqttLoopThread.start()
    
    def toggleConnection(self, button):
        if self.connected:
            t = Thread(target=self.disconnectMQTT, args=[button])
            t.start()
        else:
            t = Thread(target=self.connectMQTT, args=[button])
            t.start()
    
    def publish(self, button):
        def p(self):
            self.bottomBar.start("Publishing...")
            if not self.publishPane.topicField.valid():
                    self.bottomBar.stop("Invalid topic!")
                    return
            if self.connected:
                token = self.client.publish(
                    self.publishPane.topicField.get_text(),
                    self.publishPane.payloadField.get_text(),
                    self.publishPane.qosSelector.value(),
                    self.publishPane.retainCheckbox.get_active())
                token.wait_for_publish()
                if token.is_published():
                    self.bottomBar.stop("Published.")
                else:
                    self.bottomBar.stop("Publish failed!")
            else:
                self.bottomBar.stop("Not connected!")
        t = Thread(target=p, args=[self])
        t.start()
    def subscribe(self, button):
        self.bottomBar.start("Subscribing...")
        topicName = self.subscribePane.topicField.get_text()
        if self.connected:
            if not self.subscribePane.topicField.valid():
                self.bottomBar.stop("Invalid topic!")
                return
            try:
                self.client.subscribe(topicName, qos=self.subscribePane.qosSelector.value())
                self.subscribePane.topicField.set_text("")
                self.subscribePane.subscribe(topicName)
                self.bottomBar.stop("Subscribed successfully.")
            except:
                self.bottomBar.stop("Subscription failed!")
        else:
            self.bottomBar.stop("Not connected!")
    def unsubscribe(self, _):
        def us(self):
            self.bottomBar.start("Unsubscribing...")
            topics = self.subscribePane.subList.getSelectedTopics()
            if len(topics) > 0:
                for topic in topics:
                    self.client.unsubscribe(topic)
                GLib.idle_add(self.subscribePane.subList.removeSelected)
                self.bottomBar.stop("Unsubscribed successfully.")
            else:
                self.bottomBar.stop("No topics selected!")
        t = Thread(target=us, args=[self])
        t.start()
    def onMessage(self, cl, userdata, msg):
        self.bottomBar.start("Receiving message.")
        GLib.idle_add(self.subscribePane.messageView.appendMessage, msg.topic, msg.payload)
        self.bottomBar.stop("Received message.")
    def mqttLoop(self):
        while self.running:
            sleep(0.1)
            if self.connected:
                self.client.loop()
    
    def connectMQTT(self, button):
        self.bottomBar.start("Connecting...")
        self.client = mqtt.Client(client_id="MQTTool")
        if self.connectPane.tlsEnabled():
            self.client.tls_set(self.connectPane.caCertPath(), self.connectPane.clientCertPath(), self.connectPane.keyCertPath())
        if self.connectPane.pswdEnabled():
            self.client.username_pw_set(self.connectPane.username(), self.connectPane.password())
        try:
            self.client.connect(self.connectPane.getHost(), port=self.connectPane.getPort())
            self.connected = True
            self.client.on_message = self.onMessage
            self.setWidgetSensitivities() 
            self.bottomBar.stop("Connected successfully.")
            button.set_label("Disconnect")
        except:
            self.bottomBar.stop("Connection failed!")
    def disconnectMQTT(self, button):
        self.bottomBar.start("Disconnecting...")
        try:
            self.subscribePane.subList.select_all()
            self.unsubscribe("")
            self.client.disconnect()
            self.connected = False
            self.setWidgetSensitivities() 
            self.bottomBar.stop("Disconnected successfully.")
            button.set_label("Connect")
        except:
            self.bottomBar.stop("Disconnection failed!")
    
    def setWidgetSensitivities(self):
        self.publishPane.set_sensitive(self.connected)
        self.subscribePane.set_sensitive(self.connected)
        self.connectPane.setSensitivityOnConnState(self.connected)

    def loadStylesheet(self, path):
        cssFile = Gio.File.new_for_path(path)
        self.style = Gtk.CssProvider()
        self.style.load_from_file(cssFile)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            self.style,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
    
    def exitCleanly(self, _):
        self.running = False
        self.mqttLoopThread.join()
        if self.connected:
            self.disconnectMQTT(Gtk.Button())

window = MainWindow()
window.show_all()
Gtk.main()
