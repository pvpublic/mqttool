from custom_widgets import QosSelector, TopicEntry, WidgetBoxWithLabel
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from time import strftime

class SubscribePane(Gtk.Paned):
    def __init__(self):
        Gtk.Paned.__init__(self, orientation=Gtk.Orientation.HORIZONTAL, expand=True)

        self.leftGrid = Gtk.Grid(expand=True)
        self.rightGrid = Gtk.Grid(expand=False)

        self.topicField = TopicEntry(expand=False)
        self.leftGrid.attach(self.topicField, 1, 1, 2, 1)

        self.subscribeBtn = Gtk.Button(expand=False)
        self.subscribeBtn.set_label("Subscribe")
        self.subscribeBtn.set_border_width(5)
        self.subscribeBtn.set_size_request(-1, -1)
        self.leftGrid.attach_next_to(self.subscribeBtn, self.topicField, Gtk.PositionType.BOTTOM, 1, 1)

        self.qosSelector = QosSelector()
        self.leftGrid.attach_next_to(self.qosSelector, self.subscribeBtn, Gtk.PositionType.RIGHT, 1, 1)

        self.subList = SubscriptionList()
        self.subListScrollWindow = Gtk.ScrolledWindow(expand=True)
        self.subListScrollWindow.add_with_viewport(self.subList)
        self.subListScrollWindow.set_size_request(-1, -1)
        self.leftGrid.attach_next_to(self.subListScrollWindow, self.subscribeBtn, Gtk.PositionType.BOTTOM, 2, 1)

        self.unsubscribeBtn = Gtk.Button(expand = False)
        self.unsubscribeBtn.set_label("Unsubscribe")
        self.unsubscribeBtn.set_border_width(5)
        self.unsubscribeBtn.set_size_request(-1, -1)
        self.leftGrid.attach_next_to(self.unsubscribeBtn, self.subListScrollWindow, Gtk.PositionType.BOTTOM, 2, 1)
        self.add1(self.leftGrid)

        self.messageView = MessageViewPane()
        self.add2(self.messageView)
    
    def subscribe(self, topic):
        self.subList.append(topic)

class SubscriptionList(Gtk.ListBox):
    def __init__(self):
        Gtk.ListBox.__init__(self, expand=False)
        self.set_selection_mode(Gtk.SelectionMode.MULTIPLE)
        
    def append(self, topic):
        box = SubscriptionListRow(topic)
        self.add(box)
        self.show_all()
    
    def getSelectedTopics(self):
        topics = []
        def addTopicFromRow(subList, subListRow):
            topics.append(subListRow.topic)
        self.selected_foreach(addTopicFromRow)
        return topics
    
    def removeSelected(self):
        for row in self.get_selected_rows():
            self.remove(row)

class SubscriptionListRow(Gtk.ListBoxRow):
    def __init__(self, topic):
        Gtk.ListBoxRow.__init__(self, expand=False)
        self.topic = topic
        self.add(Gtk.Label.new(topic))

class MessageView(Gtk.ScrolledWindow):
    def __init__(self):
        Gtk.ScrolledWindow.__init__(self, expand=True)
        self.buffer = Gtk.TextBuffer()
        self.msgField = Gtk.TextView(expand=True)
        self.msgField.set_buffer(self.buffer)
        self.msgField.set_editable(False)
        self.msgField.set_name("MsgField")
        self.msgField.set_size_request(350, -1)
        self.msgField.connect('size-allocate', self.scrollToEnd)
        self.add(self.msgField)
        self.set_size_request(350, -1)
        self.setAutoscroll(True)
    
    def appendMessage(self, topic, payload):
        text = "\n\n<b>Message on {} at {}:</b>\n{}".format(topic, strftime("%H:%M:%S"), payload.decode('utf-8'))
        self.buffer.insert_markup(self.buffer.get_end_iter(), text, len(text))
    
    def setAutoscroll(self, shouldAutoscroll):
        self.autoscroll = shouldAutoscroll

    def scrollToEnd(self, widget, event, data=None):
        if self.autoscroll:
            adj = self.get_vadjustment()
            adj.set_value(adj.get_upper() - adj.get_page_size())

    def clear(self):
        self.buffer.set_text("", 0)
        self.pos = self.buffer.get_start_iter()

class MessageViewPane(Gtk.Grid):
    def __init__(self):
        Gtk.Grid.__init__(self, expand=False)

        self.messageView = MessageView()
        self.attach(self.messageView, 1, 1, 2, 1)

        self.autoscrollSwitch = Gtk.CheckButton()
        self.autoscrollSwitchBox = WidgetBoxWithLabel("Autoscroll", self.autoscrollSwitch)
        self.autoscrollSwitch.connect("clicked", self.setAutoscroll)
        self.autoscrollSwitch.set_active(True)
        self.attach_next_to(self.autoscrollSwitchBox, self.messageView, Gtk.PositionType.BOTTOM, 1, 1)

        self.clearBtn = Gtk.Button()
        self.clearBtn.connect("clicked", self.clearMsgBox)
        self.clearBtn.set_label("Clear")
        self.clearBtn.set_border_width(5)
        self.clearBtn.set_size_request(-1, -1)
        self.attach_next_to(self.clearBtn, self.autoscrollSwitchBox, Gtk.PositionType.RIGHT, 1, 1)
    
    def clearMsgBox(self, button):
        self.messageView.clear()
    def setAutoscroll(self, switch):
        self.messageView.setAutoscroll(switch.get_active())
    def appendMessage(self, topic, payload):
        self.messageView.appendMessage(topic, payload)
