# MQTTool #

A simple MQTT client GUI built with GTK and Python 3.

As of now, this is still in development, albeit usable. Simply install the dependencies, clone the repository, and run `main.py`.

![Screenshot](SubscribePane.png)

---

## Dependencies ##

- Python 3
- PyGObject
- [Eclipse Paho MQTT Library](https://pypi.org/project/paho-mqtt/)

---

## Features ##

Largely shared with those of the Paho MQTT library.

- Connection with username/password, and TLS
- Publish and subscribe with quality of service 0, 1, and 2
- Multiple subscriptions

---

## To Do ##

- MQTT 5 support
- JSON formatting in subscription view
- Notification on message arrival
- Ability to filter messages by topic
